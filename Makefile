%.html: %.md stylesheet.css
	pandoc \
		--metadata title="$$(\
			echo "$^" \
			| tr '_' ' ' \
			| sed --expression 's/\.md$$//g' --expression 's/\b\([a-z]\)/\u\1/g'\
		)" \
		--from markdown \
		--embed-resources \
		--standalone \
		--css stylesheet.css \
		--output $@ \
		$^

%.pdf: %.html
	wkhtmltopdf --encoding utf-8 $^ $@

clean:
	-rm --force *.html *.pdf
