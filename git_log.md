# git log

  - Finding out who did what using `git` via `git log`
  - `git log` shows commit logs

I use `git log` quite a bit, and find it fantastically useful since I often find
myself wondering who and why things changed so I thought I'd talk about the bits
I find useful.

See the docs:

  - <https://www.git-scm.com/docs/git-log>
  - `git help log`

<!-- Demonstrate by commiting and viewing it back -->

The git log command displays a record of the commits in a Git repository. It
tells you who did what, what they did and (possibly) why the did it (though the
last one depends on how much detail people put into their commits).

## General Useful Flags

### `--oneline`

Shows commits on one line each, giving a quick and concise overview of changes

### `-p/--patch`

Shows the diff corresponding to a commit

### `--no-merges`

Useful when you want to look at just the changes that were made

### `--merges`

Useful when you want a summary of what changed recently, a rather useful
combination is this flag with `--first-parent`:

> When finding commits to include, follow only the first parent commit upon
> seeing a merge commit. This option can give a better overview when viewing the
> evolution of a particular topic branch, because merges into a topic branch
> tend to be only about adjusting to updated upstream from time to time, and
> this option allows you to ignore the individual commits brought in to your
> history by such a merge.

E.g. it won't show merges of main into some feature branch

So for a quick overview of the work recently done you could:

``` console
$ git log --merges --oneline --first-parent
```

### `--reverse`

> Output the commits chosen to be shown in reverse order

Useful when you want to see how something has progressed, as opposed to the
default newest-\>oldesting ordering

## Paths

`git log` accepts a list of paths as positional arguments, e.g. `git log --
src/` to show only commits that changed something under `/src` (the `--` is
optional, though you might need it to disambiguate files with spaces of files
starting with `-` that could otherwise be interpreted as arguments).

See what changed in all python files:

``` console
$ git log -- '*.py'
```

## Finding Specific Changes

### Search in commit messages with `--grep`

E.g we might want to search for any commits that mention 'CVE':

``` console
$ git log --grep CVE
```

Or, if we want to see all the work done, in chronological order, against a
specific issue:

``` console
$ git log --reverse --no-merges --grep gh-95778
```

### Search for commits with specific author using `--author`

See what Guido's been up to:

``` console
$ git log --author='Guido van Rossum' 
```

See what I've been up to (using command substitution in Bash):

``` console
$ git log --author="$(git config user.name)"
```

### Search for a specific change with `-S/-G`

> \-S <string>  
> Look for differences that change the number of occurrences of the specified
> string (i.e. addition/deletion) in a file. Intended for the scripter’s use.

> \-G<regex>  
> Look for differences whose patch text contains added/removed lines that match
> <regex>.

(see also `--pickaxe-regex`)

For example, looking for imports of `tomllib` added in Python 3.11

``` console
$ git log --reverse -S 'import tomllib'
```

### Tracking The History of a funcion/range of lines/... with `-L`

3 forms:

#### `-L <start>,<end>:some_file.py`

`<start>`, `<end>` can be line numbers (though I don't use this often), or they
can be POSIX regex patterns

E.g. view the history of the constants related to the version of Python:

``` console
$ git log -L '/--start constants--/,/--end constants--/':Include/patchlevel.h
```

### `-L :<funcname>:<file>`

E.g. follow the history of the `breakpoint` builtin function:

``` console
$ git log -L:builtin_breakpoint:Python/bltinmodule.c
```

Or for `functools.reduce`

``` console
$ git log -L:reduce:Lib/functools.py
```

Note: the matching here is performed by git's diff engine, which comes with
builtin support for several languages, see
<https://www.git-scm.com/docs/gitattributes#_defining_a_custom_hunk_header>
Python is included but not some others like JS, Swift, Kotlin, you can add you
own by defining a custom hunk header (as in that link) then marking the
attributes, something like:

    *.js    diff=javascript
    *.ts    diff=javascript

Works with completion (nicely on Bash, mixed results on zsh)

### Changes within a given time range with `--since/--until`

(Equivalently `--before` `--after`), e.g. what did I do this week (get user name
from config first)

``` console
git log --merges --author="$(git config user.name)" --since='1 week'
```

(via Command Substitution)

The date fields accept e.g. ISO8601 (which you should probably use) and
human-centric date times (like above) see also
<https://git-scm.com/docs/git-commit#_date_formats>

## Formatting the Output

The default output of `git log` can sometimes be a bit verbose, `--oneline` is
often very helpful for reducing the noise, but you can be much more specific

<https://www.git-scm.com/docs/git-log#_pretty_formats>

Particularly useful when someone posts in Slack "Hey I noticed `<X>` started
behaving differently since 4 days ago" to get a quick overview of what happened
around that time:

``` console
git log --merges --first-parent --since='5 days ago' --until='3 days ago' --format="%h %an %s"
```

If anything looks relevant to `<X>` you can chase up the author

Commit leaderboard\!

``` console
$ git log --format='%an' | sort | uniq --count | sort --numeric-sort --reverse | head
  11250 Guido van Rossum
   7116 Victor Stinner
   5866 Benjamin Peterson
   5679 Georg Brandl
   5465 Fred Drake
   4420 Serhiy Storchaka
   4392 Raymond Hettinger
   3776 Antoine Pitrou
   2978 Jack Jansen
   2765 Martin v. Löwis
```

## Extras

`git log --graph --oneline`
