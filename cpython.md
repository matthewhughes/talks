# CPython

Open URLS:

  - <https://github.com/python/cpython/>
  - <https://en.wikipedia.org/wiki/CPython>
  - <https://devguide.python.org/#other-interpreter-implementations>
  - <https://devguide.python.org/exploring/>
  - <https://docs.python.org/3/library/functions.html#any>

Who am I?\!

Who am I *not*: A CPython dev, so trust nothing I claim :\>

Let's talk about CPython

Point of this talk

  - Introduction to CPython

  - Take a peak under the hood of CPython
    
      - Poke around some Python modules
    
      - Learn how to look up your favourite module in the stdlib (maybe)
    
      - Poke around some C modules too\!
    
      - Compare a function at C-level vs how the python level might look

  - Add a new builtin function `lyst` that's just an alias for `list`

  - (If time poke around some easter eggs)

  - Here it is <https://github.com/python/cpython/>

  - When you run `python` on your machine you're running CPython

## Poking around the repo

  - It has some great documentation (links in the README)

  - You can build it yourself\!
    
      - Build instructions, including how to install dependencies, in the dev
        docs
      - But you probably don't want to run the version you build yourself as a
        replacement of the system packaged version since there are several
        toggles you can set while building CPython, and it's easiest to just
        trust the ones set by the package maintainers

  - Note other implementations,
    <https://devguide.python.org/#other-interpreter-implementations>

  - Reasons for C modules
    
      - Performance
      - Need to interact with system, e.g. `time`

  - <https://devguide.python.org/exploring/#exploring-cpython-s-internals>
    
      - Visit `Lib/argparse`, pure Python
      - Visit `Lib/operator.py`, interesting because it has both pure Python and
        C implementations, also visit `Modules/_operator.c` -
        `_operator_lt_impl`
      - Note there is no `Lib/itertools.py` (extension-only module), visit
        `modules/itertools.c`: `static struct PyModuleDef itertoolsmodule`

  - Poke around a Python function, e.g. working with `urllib.parse.urlencode`

  - Example simple C function: maybe `builtin_all`

<!-- end list -->

``` python
from collections.abc import Iterable

def any(iterable: Iterable[object]) -> bool:
    for element in iterable:
        if element:
            return True
    return False
```

Syntactically, we can't really `for element in iterable` in C, so instead

``` python
from collections.abc import Iterable

def any(iterable: Iterable[object]) -> bool:
    iterator = iter(iterable)

    while True:
        try:
            element = next(iterator)
        except StopIteration:
            break
        if element:
            return True

    return False
```

  - In C you have to keep count of how many times a variable is referenced
  - Some of the error handling we'll see:
      - A functioning returning a pointer: returning NULL signals an error
      - A function returning an int: returning a negative value signals an error

Look at `Python/bltinmodule.c:builtin_any`

## Let's Make a change and build it ourselves

Copy `list` as `lyst`, via `_PyBuiltin_Init`

## Misc Notes

Accessing pure-python implementations:

    >>> import sys
    >>> sys.modules["_datetime"] = None
    >>> import datetime
    >>> datetime.datetime.now
    <bound method datetime.now of <class 'datetime.datetime'>>

A fun "Hello world" program

    $ python -m __hello__
    Hello world!
    
    $ python -m this

vs inspecting `Lib/this.py`

An XKCD joke:

    $ python -m antigravity
